## Description

This is a bootstrap template that list some common french expressions with their literal meaning and what really means to say. Along with that a small google translate example for 'speaking' the phrase.

## Techs
* Bootstrap 
* JS 
* Google Translate Integration 
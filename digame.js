var phrases = [
{phrase : "Perdre la boule", literal:'"To lose your ball"', meaning:"To lose your head"},
{phrase : "Ce n'est pas la mer à boire", literal:'"It\'s not like you have to drink the ocean"', meaning:"It's not hard to do"},
{phrase : "Faire la grasse matinée", literal:'"To have a fat morning"', meaning:"To sleep in"},
{phrase : "Passer une nuit blanche", literal:'"To have a white night"', meaning:"To stay up/awake all night (usually on purpose, not because of insomnia)"},
{phrase : "Passer un mauvais quart d'heure", literal:'"To have a bad quarter of an hour"', meaning:"A short, difficult period of one's life"},
{phrase : "Dormir à la belle étoile", literal:'"To sleep in the pretty star"', meaning:"To sleep outside"},
{phrase : "Être blanc comme neige", literal:'"To be as white as snow"', meaning:"To be completely innocent"},
{phrase : "Faire boule de neige", literal:'"To make like a ball of snow"', meaning:"To get bigger / more important"},
{phrase : "Faire du / son chemin", literal:'"To go along the/one\'s path"', meaning:"To make progress"},
{phrase : "Mordre la poussière", literal:'"To bite the dust"', meaning:"To suffer a defeat"},
{phrase : "Traîner quelqu'un dans la boue", literal:"To drag someone through the mud", meaning:"To dirty someone's reputation"}
]

function play() {
    if(audioTag.paused) {
        audioTag.play();
        playButton.className = 'fa fa-pause animated';
    } else {
        audioTag.pause();
        playButton.className = 'fa fa-play animated';
    }
}

function onPause() {
    playButton.className = 'fa fa-refresh animated';
}

function createAudioURL(phrase, lang) {
    return 'http://translate.google.com/translate_tts?tl=' + lang + '&client=tw-ob&q='+ phrase + '&ie=UTF-8';
}

function changePhrase(ph, li, me) {
    phraseContainer.innerHTML = phrase;
    literalContainer.innerHTML = literal;
    meaningContainer.innerHTML = '<strong>What really means:</strong> ' + meaning;

    ga('send', {phrase: ph, literal: li, meaning: me});
    var newURL = encodeURI(createAudioURL(phrase, 'fr'));
    audioTag.src = newURL.toLowerCase();
    console.debug( encodeURI(createAudioURL(phrase, 'fr')));
}

function changePhraseByIndex(content,index) {
    var current = content[index];
    changePhrase(current.phrase, current.literal, current.meaning);
    playButton.className = 'fa fa-play animated';

    setTimeout(play.bind(this),1000);
}
function next() {
    if( currentIndex + 1 < phrases.length) {
        currentIndex = currentIndex + 1;
        changePhraseByIndex(phrases,currentIndex); 
    }
    ga('send', {
      hitType: 'click',
      eventAction: 'next',
    });
    updateNextPrevious();
    updateNextPrevious();
}

function previous() {
    if( currentIndex - 1 >= 0) {
        currentIndex = currentIndex - 1;
        changePhraseByIndex(phrases,currentIndex); 
    }
    ga('send', {
      hitType: 'click',
      eventAction: 'previous',
    });
    updateNextPrevious();
}

function updateNextPrevious() {
    if(currentIndex > phrases.length) {
        nextButton.setAttribute('disabled', 'disabled');
    } else {
        nextButton.removeAttribute('disabled');
    }

    if(currentIndex <= 0) {
        previousButton.setAttribute('disabled', 'disabled');
    } else {
        previousButton.removeAttribute('disabled');
    }
}

window.onkeyup = function(e) {
   var key = e.keyCode ? e.keyCode : e.which;
   console.debug(key);
   if (key == 39) {
       next();
   }else if (key == 37) {
       previous();
   }
}

var nextClicks = 0;
var previousClicks = 0;
var currentIndex = 0;
var audioTag = document.getElementsByTagName('audio')[0];
var playButton = document.getElementById('playButton');
var phraseContainer = document.getElementById('phrase');
var literalContainer = document.getElementById('literal');
var meaningContainer = document.getElementById('meaning');
var nextButton = document.getElementById('nextButton');
var previousButton = document.getElementById('previousButton');
audioTag.addEventListener('pause',onPause.bind(this));

changePhraseByIndex(phrases,currentIndex);
updateNextPrevious();
